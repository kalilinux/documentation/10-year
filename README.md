## 10 Year

**Kali**

A special thanks to our sponsor: The Arg Society. We are so grateful to a group of ArgSOC members for taking the time over the past few months to design and build logical puzzles for this event. You've worked so hard and we want to show appreciation and recognition to @Carnage, who did the amazing graphics, @speexvocon & @Sal3m, who led the puzzle creation team that includes: @cr4mb0, @Willvoid, @RipVanWinkle, @sawtooth, @obafgkm, @EizAKme, @Beamofoldlight, and @discursiveaura Catalytic Chaos Coordinator, and the rest of the ARGSOC Contributor. A big shout-out goes to the Kali Linux contributors for testing the puzzles during the pre-prod stage: @ApexPredator, @c0ntra, @greenjam94, @snowcrash, @XORW3LL, @ArcticCow, @ShadowKhan, @Tristram, @Arszilla. As a result of your hard work, the event went forward with a bang.

Finally, thanks to the Kali Linux team @daniruiz, and especially @KyTiX, OffSec's senior developer, who played a crucial role in the event's success. Without our sponsors and community moderators, this event would not be possible!

- - -

**ARGSOC**

On behalf of everyone here at ARGSOC, I'd like to firstly thank the folks at OffSec and Kali for giving us the opportunity of working with them on this project. Everyone involved brought their "A game" and we couldn't be more proud of the outcome. We look forward to bigger and better collaborations on future events.
To the winners, a well deserved congratulations. The speed and ingenuity shown to overcome the challenge was, in a word, breathtaking. Well done.

To anyone who took part in the challenge, our sincere thanks and appreciation. We hope you enjoyed the event as much as we did creating it.
