- Puzzle name: ANSI BBS -> BBS 1995 -> `eps1.0_BBS-1995.mov`
- Contributor: sawtooth
- Puzzle solution: `WHOPPIX`
- Puzzle steps:
> An ANSI BBS looking page with menus and a list of 7 online users and a hint highlighted in red under the recent file uploads list. Each user has a connection speed, like in the old modem days, the link speed would be looked up in a list of modem AT commands and converted to number; `Speed 50666bps = AT&N26 = Z`, or `300 bps = AT&N1 = A`
>
> The list of user’s link speeds could be looked up and converted to numbers for alphabet A-Z from a site like <https://support.usr.com/support/3cxm756/3cxm756-ug/atcoms.htm>
>
> Hint: having a part of the BBS saying "latest file upload - USR Modem AT Commands" which Google searching results in the link above.

![](preview1.png)
