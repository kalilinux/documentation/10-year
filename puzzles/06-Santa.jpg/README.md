- Puzzle name: Santa.jpg -> `eps1.6_S4nt4.jpg`
- Contributor: Eizak/Disc
- Puzzle solution: `Nethunter`
- Puzzle steps:
> Image is of Santa Claus World (explained in SATAN [article excerpt](https://www.latimes.com/archives/la-xpm-1995-05-14-tm-510-story.html) on the right side), Linnanmaki Theme Park, Helsinki Finland (Where Linus Torvalds was born, also where the location metadata points) applied a filter to alter the image so that the specific source image doesn't immediately appear in reverse image search as simply, since it's not relevant to the puzzle
>
> _Metadata+[Steg](https://futureboy.us/stegano/decinput.html) (hints in metadata that its a steg puzzle)_
>
> Several clues in the metadata of the image to Linus Torvald (birthday, linux creation date, university of helsinki), along with with more blatant references to one of the first pentesting tools called Security Administrator Tool for Analyzing Networks ([SATAN](https://www.latimes.com/archives/la-xpm-1995-05-14-tm-510-story.html), 1995) and it's creators, Dan Farmer, and Wietse Zweitze Venema (formatted as W1ETSEZWE1TZEVENEMA as a clue for the steg password format)
- Puzzle answer phrase: L1NUSBENED1CTTORVALDS (steg password)
- Puzzle Preview: Santa.jpg

![](preview1.jpeg)

![](preview2.jpeg)

![](preview3.jpeg)

![](preview4.jpeg)
