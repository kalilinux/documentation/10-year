- Puzzle name: Kismet -> `eps1.1_ki5met.mpeg`
- Contributor: Sal3m
- Puzzle solution: `Lovelace`
- Puzzle steps:
> The packets are the fractionated Morse encoded text: `DHOAPTCFKG`
>
> The network is `aca1960` and the BSSID is the hex of fractionated combine the two to get the cryptocrack magazine which has fractionated Morse cipher in
>
> Decode the Morse encoded text to get `lovelace`
- Puzzle Preview: Kali.wav

![](preview1.png)
