- Puzzle name: RGB Kali Dragon -> `eps1.5_RG8.asf`
- Contributor: obafgkm
- Puzzle solution: `MULTIPLEXING`
- Puzzle steps:
> Separate the image into red, green, blue channels and decode from binary; red channel gives instructions to XOR the other channels together and flip all the bits, resulting in the puzzle answer

![](preview1.png)
