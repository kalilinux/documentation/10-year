- Puzzle name: Logic Gates -> Diagram Deduction -> `eps1.3_dia6r4m.mp4`
- Contributor: Beamofoldlight
- Puzzle solution: `SECURITY`
- Puzzle steps:
> The basic layout is that you use the second row of letters to understand the pattern of the puzzle. The first row of letters are only there so you can work out how the gates work. If familiar or with basic research, one can assume the basic function of the NOT and AND gates. For the "NOT" operator, count the amount of spaces it is from the front or back, and then count that number from the opposite side. Make sure you're counting/zero indexing and not subtracting. For AND, add the values of the letters together.
>
> **Light blue** are resistors hence `-1`. **Orange** are inductors hence `+1`. **Green** are variable resistors using the input of one to subtract from the other. Players without any electrical component knowledge would only need to google the symbol and grasp the base concept of what is does. As for the logic gates AND speaks for itself. NOT is also called an Inverter and should hint the player to flip in the alphabet.

![](preview1.png)
