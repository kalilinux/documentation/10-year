- Puzzle name: Tv Static -> `eps1.4_7V-Static.wmv`
- Contributor: Cr4mb0
- Puzzle solution: `SWORDFISH`
- Puzzle steps:
> Animated GIF with two non-black TV screen frames that appear to be static. When merged/interlaced, the word `SWORDFISH` can be seen.

![](preview1.png)
