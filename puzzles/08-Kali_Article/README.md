- Puzzle name: Kali Article -> `eps1.8_ARTIC1E.m4v`
- Contributor: Rip
- Puzzle solution: `dookieandmuts`
- Puzzle steps:
> Very rough draft - I’d like this to look like a physical newspaper headline/story with photos embedded. May also be hitting people over the head clumsily with the hints at the end but wanted to get some initial feedback on this
>
> **KALI TURNS TEN** (In Kali Blog Font – if possible)
>
> <Kali Anniversary Image, if one is planned? Or a photo of the Kali Team Celebrating?>
>
> Milestone Tenth Anniversary for Pen Testing: Kali Linux OS
>
> You have to humorously equate the ride that the starters of a crew - such as Kali & OffSec - just launch into headlong with that of mad scientists painstakingly creating theoretical cure-alls before testing them on themselves. They invested countless hours of research, development, and testing in an attempt to make life better and easier for others while accepting the risk that it may all lead to nothing in the end. Be that as it may, OffSec’s Kali Linux has proved to be an indispensable and evolving solution in the toolbox of modern penetration testers and Information Security Professionals
>
> To understand the solution that Kali has become, you need to go back to the very beginning and count the iterations involved that led to this 10th Anniversary. It was first showcased at Black Hat Europe on March 13th, 2013 and boasted over 300 pen testing and security auditing tools that provided a one-stop-shop for IT Administrators and IT Professionals alike. Now, instead of painstakingly searching for – or developing from scratch – solutions to identify weaknesses, those looking to improve the security posture of their systems could simply fire up Kali and get started.
>
> "For IT professionals, an experiment is worth a thousand theories" said Mati Aharoni. "Applied to security, it means that simulating attacks to assess the defences protecting your organization is the only sure way to understand their effectiveness and the impact of an attack." And Kali, with its powerful tools like Fluxion, John the Ripper, Lynis, Aircrack-ng, Hydra, Metasploit, and countless more, empowers IT Professionals to perform real world experiments to identify and address issues with relative ease.
>
> As the founders of Kali have shown throughout their journey, you may need to count every letter from the first to the 10th degree just to get started. On the other hand, sometimes the first sentence of a piece completes the picture and the rest of the article is simply minutiae. And while it may not make sense at first, rotating that first piece in your mind to view it from different perspectives may eventually lead to a key breakthrough like the founders of Kali discovered.
- CIPHER: teeayuqdtckji
- ROT10: dookieandmuts

![](preview1.png)
