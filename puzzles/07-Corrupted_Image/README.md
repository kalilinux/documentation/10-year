- Puzzle name: Corrupted Image -> `eps1.7_co22upted.flv`
- Contributor: Willvoid
- Puzzle solution: `WETTROUSERS`
- Puzzle steps:
> <https://jsfiddle.net/x7fdg51a/4/show>
>
> (involves html/css as a heads up for dev team)
>
> Idea is that screen has corrupted text and the minimal hint is the use of the Kali tool, Foremost to hint at you have to recover data from a corrupted image, in this case a literal corrupted png file. The other hint is the coloured text is highlighting decimal values that are converted into ASCII table values of ZALGO in case that helps in determining to decode this zalgo text into proper form. Player has to select all text (not sure if possible on mobile) and take it to a Zalgo decoder then what you end up is with base64 text that has to be converted into an image (an online tool is available to do both actions) and once the player reads the text on the image, that’s the solution. WETTROUSERS
- Puzzle Preview: Corrupted Image

![](preview1.gif)
