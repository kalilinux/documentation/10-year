- Puzzle name: BackTrace Commercial -> `eps1.2_BT-adv3rt.mkv`
- Contributor: Cr4mb0
- Puzzle solution: `KALI4KIDS`
- Puzzle steps:
> Video with hidden text in audio, viewable via spectrogram analysis.

![](preview1.png)
